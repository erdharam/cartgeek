<?php
class LeftBar
{
	public function showMenu()
	{
		$this->widget('zii.widgets.jui.CJuiAccordion', array(
			'panels'=>array(
			
			'Manage Admin'=>
			'<ul class="leftbar_menu">
			<li><a class="home" href="' . Yii::app()->createUrl("private/admin/index") . '">Home</a></li>
			<li><a class="bg" href="' . Yii::app()->createUrl("private/admin/update/id/".Yii::app()->user->user_id) . '">Change Admin Password</a></li>
			<li><a class="bg" href="' . Yii::app()->createUrl("private/admin/addlinks/").'">Contact Email </a></li>
			<li><a class="bg" href="' . Yii::app()->createUrl("private/admin/contactno/").'">Contact Number</a></li>
			<li><a class="logout" href="' . Yii::app()->createUrl("private/admin/logout") . '">Logout</a></li>
			</ul>',
			
			'Manage City & Area'=>
			'<ul class="leftbar_menu">
			<li><a class="home" href="' . Yii::app()->createUrl("private/iconCityTbl/create") . '">Add City</a></li>
			<li><a class="home" href="' . Yii::app()->createUrl("private/iconCityTbl/admin") . '">All Cities</a></li>
			<li><a class="home" href="' . Yii::app()->createUrl("private/cityareatbl/create") . '">Add Area</a></li>
			<li><a class="home" href="' . Yii::app()->createUrl("private/cityareatbl/admin") . '">All Area</a></li>
		    </ul>',
				
			'Manage Service Vendor'=>
			'<ul class="leftbar_menu">
			<li><a class="home" href="' . Yii::app()->createUrl("private/vendorinfo/create") . '">Add Vendor</a></li>
			<li><a class="home" href="' . Yii::app()->createUrl("private/vendorinfo/admin") . '">All Vendors</a></li>
			
		    </ul>',
			'Manage Site Visit'=>
			'<ul class="leftbar_menu">
			<!--<li><a class="home" href="' . Yii::app()->createUrl("private/iconsitevisit/create") . '">Add Vendor</a></li>-->
			<li><a class="home" href="' . Yii::app()->createUrl("private/iconsitevisit/admin") . '">All Visit Request</a></li>
			
		    </ul>',
				
				'Manage Site Visit'=>
			'<ul class="leftbar_menu">
			<!--<li><a class="home" href="' . Yii::app()->createUrl("private/iconsitevisit/create") . '">Add Vendor</a></li>-->
			<li><a class="home" href="' . Yii::app()->createUrl("private/iconsitevisit/admin") . '">All Visit Request</a></li>
			
		    </ul>',		   
				),
				// additional javascript options for the accordion plugin
				'options'=>array(
					'animated'=>'bounceslide',
					'autoHeight'=> false,
					'navigation'=> true,
				),
		));
	}
}
?>