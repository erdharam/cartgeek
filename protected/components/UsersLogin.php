<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UsersLogin extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	 
	private $_id; 
	 
	public function authenticate()
	{
		
		 $user=User::model()->findByAttributes(array('memberEmail'=>$this->username,'memberPass'=>$this->password));
        if($user===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if($user->memberPass!==$this->password)
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else
        {
            $this->_id=$user->id;
            $this->setState('user_name', $user->memberEmail);
			$this->setState('userid', $user->id);
			//$this->setState('role', $user->role);
			$this->errorCode=self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
 
    public function getId()
    {
        return $this->_id;
    }
}
