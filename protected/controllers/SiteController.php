<?php

class SiteController extends Controller

{

	public $totRating;	
	public $layout='//layouts/sitemain';

	

 public function accessRules()

{

		return array(

			array('allow',  // allow all users to perform 'index' and 'view' actions 

				'actions'=>array('about','privacy','index','login','contact','logout','bricks','putty','cement','pop','paints','morang','marble','sariya','gitti','tiles','terms','thankyou','blog','starRatingAjax','latest','services','popularnews','subcategory','error','blogs','post','category','error','brickDetail','cementDetail','sariyaDetail','puttyDetail','popDetail','morangDetail','tilesDetail','paintsDetail','marbleDetail','gittiDetail','cementDetail','brick-detail',),

				'users'=>array('*'),

			),



			array('deny',  // deny all users

				'users'=>array('*'),

			),

		);

	}



	public function filters()

	{

		return array(

			'accessControl', // perform access control for CRUD operations

		);

	}

	/**

	 * Declares class-based actions.

	 */

		public function actions()

		{

		return array(

		// captcha action renders the CAPTCHA image displayed on the contact page

		'captcha'=>array(

		'class'=>'CCaptchaAction',

		'backColor'=>0xFFFFFF,

		),

		// page action renders "static" pages stored under 'protected/views/site/pages'

		// They can be accessed via: index.php?r=site/page&view=FileName

		'page'=>array(

		'class'=>'CViewAction',

		),

		);

		}

	
		public function actionSitemapcreator()
		{

			$this->render('sitemapcreator');

		}


	
		public function actionIndex()
		{

		$this->pageTitle = Yii::app()->name . ' :: Home';
		$brik = "select * from  tbl_bricks order by rand() limit 0,2 ";
		$brickdata=Yii::app()->db->createCommand($brik)->queryAll();

		$cement = "select * from  tbl_cement order by rand() limit 0,2 ";
		$cementdata=Yii::app()->db->createCommand($cement)->queryAll();
		
		$gitti = "select * from  tbl_gitti order by rand() limit 0,2 ";
		$gittidata=Yii::app()->db->createCommand($gitti)->queryAll();
		
		$marble = "select * from  tbl_marble order by rand() limit 0,2 ";
		$marbledata=Yii::app()->db->createCommand($marble)->queryAll();
		
		$morang = "select * from  tbl_morang order by rand() limit 0,2 ";
		$morangdata=Yii::app()->db->createCommand($morang)->queryAll();
		
		$paints = "select * from  tbl_paints order by rand() limit 0,2 ";
		$paintsdata=Yii::app()->db->createCommand($paints)->queryAll();
		
		$pop = "select * from  tbl_pop order by rand() limit 0,2 ";
		$popdata=Yii::app()->db->createCommand($pop)->queryAll();
		
		$putty = "select * from  tbl_putty order by rand() limit 0,2 ";
		$puttydata=Yii::app()->db->createCommand($putty)->queryAll();
		
		$sariya = "select * from tbl_sariya order by rand() limit 0,2 ";
		$sariyadata=Yii::app()->db->createCommand($sariya)->queryAll();
		
		$tiles = "select * from tbl_tiles order by rand() limit 0,2 ";
		$tilesadata=Yii::app()->db->createCommand($tiles)->queryAll();
		
		
		$brik1 = "select * from  tbl_bricks order by rand()  limit 0,20 ";
		$brickdata1=Yii::app()->db->createCommand($brik1)->queryAll();

		$cement1 = "select * from  tbl_cement order by rand() limit 0,10 ";
		$cementdata1=Yii::app()->db->createCommand($cement1)->queryAll();
		
		$gitti1 = "select * from  tbl_gitti order by rand() limit 0,10 ";
		$gittidata1=Yii::app()->db->createCommand($gitti1)->queryAll();
		
		
		$this->render('index',array('brickdata'=>$brickdata,'gittidata'=>$gittidata,'cementdata'=>$cementdata,'morangdata'=>$morangdata,'paintsdata'=>$paintsdata,'popdata'=>$popdata,'puttydata'=>$puttydata,'sariyadata'=>$sariyadata,'tilesadata'=>$tilesadata,'marbledata'=>$marbledata,'brickdata1'=>$brickdata1,'cementdata1'=>$cementdata1,'gittidata1'=>$gittidata1));

		}
		
		public function actionError()
		{
		if($error=Yii::app()->errorHandler->error)
		{
			$this->render('error');
		}
		}

	/**

	 * This is the action to handle external exceptions.

	 */

	public function actionBricks()
	{
		$recordsonpage=10;$matching="";$moesarray="";
		if(empty($_GET['page']))
		{
		$getpage=1;
		$url=Yii::app()->createUrl('site/bricks');	
		}else
		{
		$getpage=$_GET['page'];
		$url=Yii::app()->createUrl('site/bricks');	
		}
		$start=($getpage-1)*$recordsonpage;
		
		$matching .= "select * from  tbl_bricks where productType='Brick' ";

		$model2=Yii::app()->db->createCommand($matching)->queryAll();
		$totalrecords=count($model2);
		$pages2=ceil($totalrecords/$recordsonpage); 
		
		if(!empty($_GET['pricemin']))
		{
			$matching .=" and  price between '".$_GET['pricemin']."' and '".$_GET['pricemax']."' ";
		}
		
		if(!empty($_GET['search_text']))
		{
			$matching.=" and ( productType LIKE '%".addslashes($_GET['search_text'])."%' OR prodcutName LIKE '%".addslashes($_GET['search_text'])."%' OR brickType LIKE '%".addslashes($_GET['search_text'])."%' OR brandname LIKE '%".addslashes($_GET['search_text'])."%'  OR product_detail LIKE '%".addslashes($_GET['search_text'])."%' OR brickMaterial LIKE '%".addslashes($_GET['search_text'])."%') ";
		}
		
		
		if(!empty($_GET['sorttype']))
		{
				if($_GET['sorttype']==1)
				{
				$matching .="    order by price asc ";
				}
				if($_GET['sorttype']==2)
				{
				$matching .="    order by price desc ";
				}


		}
		else
		{
			$matching .="   order by price asc ";
		}
		$matching .="  limit " . $start . ", " . $recordsonpage . "  ";
		
		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();

		

		$this->render('bricks',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	}
	 
	public function actionCement()
	{
		$recordsonpage=10;$matching="";$moesarray="";
		if(empty($_GET['page']))
		{
		$getpage=1;
		$url=Yii::app()->createUrl('site/cement');	
		}else
		{
		$getpage=$_GET['page'];
		$url=Yii::app()->createUrl('site/cement');	
		}
		$start=($getpage-1)*$recordsonpage;
		
		$matching .= "select * from tbl_cement where productType='Cement' ";

		$model2=Yii::app()->db->createCommand($matching)->queryAll();
		$totalrecords=count($model2);
		$pages2=ceil($totalrecords/$recordsonpage); 
		
		if(!empty($_GET['pricemin']))
		{
			$matching .=" and  price between '".$_GET['pricemin']."' and '".$_GET['pricemax']."' ";
		}
		
		if(!empty($_GET['search_text']))
		{
			$matching.=" and ( productType LIKE '%".addslashes($_GET['search_text'])."%' OR prodcutName LIKE '%".addslashes($_GET['search_text'])."%' OR cementGrade LIKE '%".addslashes($_GET['search_text'])."%' OR brandname LIKE '%".addslashes($_GET['search_text'])."%'  OR product_detail LIKE '%".addslashes($_GET['search_text'])."%') ";
		}
		
		
		if(!empty($_GET['sorttype']))
		{
				if($_GET['sorttype']==1)
				{
				$matching .="    order by price asc ";
				}
				if($_GET['sorttype']==2)
				{
				$matching .="    order by price desc ";
				}


		}
		else
		{
			$matching .="   order by price asc ";
		}
		$matching .="  limit " . $start . ", " . $recordsonpage . "  ";
		
		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();


		$this->render('cement',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	} 
	
	public function actionGitti()
	{
		$recordsonpage=10;$matching="";$moesarray="";
		if(empty($_GET['page']))
		{
		$getpage=1;
		$url=Yii::app()->createUrl('site/gitti');	
		}else
		{
		$getpage=$_GET['page'];
		$url=Yii::app()->createUrl('site/gitti');	
		}
		$start=($getpage-1)*$recordsonpage;
		
		$matching .= "select * from tbl_gitti where productType='Gitti' ";

		$model2=Yii::app()->db->createCommand($matching)->queryAll();
		$totalrecords=count($model2);
		$pages2=ceil($totalrecords/$recordsonpage); 
		
		if(!empty($_GET['pricemin']))
		{
			$matching .=" and  price between '".$_GET['pricemin']."' and '".$_GET['pricemax']."' ";
		}
		
		if(!empty($_GET['search_text']))
		{
			$matching.=" and ( productType LIKE '%".addslashes($_GET['search_text'])."%' OR prodcutName LIKE '%".addslashes($_GET['search_text'])."%' OR gittyType LIKE '%".addslashes($_GET['search_text'])."%' OR brandname LIKE '%".addslashes($_GET['search_text'])."%'  OR product_detail LIKE '%".addslashes($_GET['search_text'])."%' OR size LIKE '%".addslashes($_GET['search_text'])."%') ";
		}
		
		
		if(!empty($_GET['sorttype']))
		{
				if($_GET['sorttype']==1)
				{
				$matching .="    order by price asc ";
				}
				if($_GET['sorttype']==2)
				{
				$matching .="    order by price desc ";
				}


		}
		else
		{
			$matching .="   order by price asc ";
		}
		$matching .="  limit " . $start . ", " . $recordsonpage . "  ";
		
		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();


		$this->render('gitti',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	} 
	
	public function actionMarble()
	{
		$recordsonpage=10;$matching="";$moesarray="";
		if(empty($_GET['page']))
		{
		$getpage=1;
		$url=Yii::app()->createUrl('site/marble');	
		}else
		{
		$getpage=$_GET['page'];
		$url=Yii::app()->createUrl('site/marble');	
		}
		$start=($getpage-1)*$recordsonpage;
		
		$matching .= "select * from tbl_marble where productType='Marble' ";

		$model2=Yii::app()->db->createCommand($matching)->queryAll();
		$totalrecords=count($model2);
		$pages2=ceil($totalrecords/$recordsonpage); 
		
		if(!empty($_GET['pricemin']))
		{
			$matching .=" and  price between '".$_GET['pricemin']."' and '".$_GET['pricemax']."' ";
		}
		
		if(!empty($_GET['search_text']))
		{
			$matching.=" and ( productType LIKE '%".addslashes($_GET['search_text'])."%' OR productName LIKE '%".addslashes($_GET['search_text'])."%'  OR brandname LIKE '%".addslashes($_GET['search_text'])."%'  OR product_detail LIKE '%".addslashes($_GET['search_text'])."%' OR marbleForm LIKE '%".addslashes($_GET['search_text'])."%' OR marbleshape LIKE '%".addslashes($_GET['search_text'])."%' OR marbleType LIKE '%".addslashes($_GET['search_text'])."%' OR marbleMaterial LIKE '%".addslashes($_GET['search_text'])."%') ";
		}
		
		
		if(!empty($_GET['sorttype']))
		{
				if($_GET['sorttype']==1)
				{
				$matching .="    order by price asc ";
				}
				if($_GET['sorttype']==2)
				{
				$matching .="    order by price desc ";
				}


		}
		else
		{
			$matching .="   order by price asc ";
		}
		$matching .="  limit " . $start . ", " . $recordsonpage . "  ";
		
		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();


		$this->render('marble',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	} 

	public function actionMorang()
	{
		$recordsonpage=10;$matching="";$moesarray="";
		if(empty($_GET['page']))
		{
		$getpage=1;
		$url=Yii::app()->createUrl('site/morang');	
		}else
		{
		$getpage=$_GET['page'];
		$url=Yii::app()->createUrl('site/morang');	
		}
		$start=($getpage-1)*$recordsonpage;
		
		$matching .= "select * from tbl_morang where productType='Morang' ";

		$model2=Yii::app()->db->createCommand($matching)->queryAll();
		$totalrecords=count($model2);
		$pages2=ceil($totalrecords/$recordsonpage); 
		
		if(!empty($_GET['pricemin']))
		{
			$matching .=" and  price between '".$_GET['pricemin']."' and '".$_GET['pricemax']."' ";
		}
		
		if(!empty($_GET['search_text']))
		{
			$matching.=" and ( productType LIKE '%".addslashes($_GET['search_text'])."%' OR productName LIKE '%".addslashes($_GET['search_text'])."%'  OR brandname LIKE '%".addslashes($_GET['search_text'])."%'  OR product_detail LIKE '%".addslashes($_GET['search_text'])."%') ";
		}
		
		
		if(!empty($_GET['sorttype']))
		{
				if($_GET['sorttype']==1)
				{
				$matching .="    order by price asc ";
				}
				if($_GET['sorttype']==2)
				{
				$matching .="    order by price desc ";
				}


		}
		else
		{
			$matching .="   order by price asc ";
		}
		$matching .="  limit " . $start . ", " . $recordsonpage . "  ";
		
		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();


		$this->render('morang',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	} 
	
	public function actionPaints()
	{
		$recordsonpage=10;$matching="";$moesarray="";
		if(empty($_GET['page']))
		{
		$getpage=1;
		$url=Yii::app()->createUrl('site/paints');	
		}else
		{
		$getpage=$_GET['page'];
		$url=Yii::app()->createUrl('site/paints');	
		}
		$start=($getpage-1)*$recordsonpage;
		
		$matching .= "select * from tbl_paints where productType='Paints' ";

		$model2=Yii::app()->db->createCommand($matching)->queryAll();
		$totalrecords=count($model2);
		$pages2=ceil($totalrecords/$recordsonpage); 
		
		if(!empty($_GET['pricemin']))
		{
			$matching .=" and  price BETWEEN  '".$_GET['pricemin']."' and '".$_GET['pricemax']."' ";
		}
		
		if(!empty($_GET['search_text']))
		{
			$matching.=" and ( productType LIKE '%".addslashes($_GET['search_text'])."%' OR productName LIKE '%".addslashes($_GET['search_text'])."%' OR brandname LIKE '%".addslashes($_GET['search_text'])."%' OR product_detail LIKE '%".addslashes($_GET['search_text'])."%' OR paintype LIKE '%".addslashes($_GET['search_text'])."%'   OR paint_color LIKE '%".addslashes($_GET['search_text'])."%'  OR product_code LIKE '%".addslashes($_GET['search_text'])."%') ";
		}
		
		
		if(!empty($_GET['sorttype']))
		{
				if($_GET['sorttype']==1)
				{
				$matching .="    order by price asc ";
				}
				if($_GET['sorttype']==2)
				{
				$matching .="    order by price desc ";
				}


		}
		else
		{
			$matching .="   order by price asc ";
		}
		$matching .="  limit " . $start . ", " . $recordsonpage . "  ";
		
		//echo $matching;
		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();


		$this->render('paints',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	} 
	
	public function actionPop()
	{
		$recordsonpage=10;$matching="";$moesarray="";
		if(empty($_GET['page']))
		{
		$getpage=1;
		$url=Yii::app()->createUrl('site/pop');	
		}else
		{
		$getpage=$_GET['page'];
		$url=Yii::app()->createUrl('site/pop');	
		}
		$start=($getpage-1)*$recordsonpage;
		
		$matching .= "select * from tbl_pop where productType='POP' ";

		$model2=Yii::app()->db->createCommand($matching)->queryAll();
		$totalrecords=count($model2);
		$pages2=ceil($totalrecords/$recordsonpage); 
		
		if(!empty($_GET['pricemin']))
		{
			$matching .=" and  price between '".$_GET['pricemin']."' and '".$_GET['pricemax']."' ";
		}
		
		if(!empty($_GET['search_text']))
		{
			$matching.=" and ( productType LIKE '%".addslashes($_GET['search_text'])."%' OR productName LIKE '%".addslashes($_GET['search_text'])."%' OR brandname LIKE '%".addslashes($_GET['search_text'])."%' OR product_detail LIKE '%".addslashes($_GET['search_text'])."%' OR popColor LIKE '%".addslashes($_GET['search_text'])."%'   OR itemwght LIKE '%".addslashes($_GET['search_text'])."%'  OR pro_sku LIKE '%".addslashes($_GET['search_text'])."%') ";
		}
		
		
		if(!empty($_GET['sorttype']))
		{
				if($_GET['sorttype']==1)
				{
				$matching .="    order by price asc ";
				}
				if($_GET['sorttype']==2)
				{
				$matching .="    order by price desc ";
				}


		}
		else
		{
			$matching .="   order by price asc ";
		}
		$matching .="  limit " . $start . ", " . $recordsonpage . "  ";
		
		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();


		$this->render('pop',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	} 
	
	public function actionPutty()
	{
		$recordsonpage=10;$matching="";$moesarray="";
		if(empty($_GET['page']))
		{
		$getpage=1;
		$url=Yii::app()->createUrl('site/putty');	
		}else
		{
		$getpage=$_GET['page'];
		$url=Yii::app()->createUrl('site/putty');	
		}
		$start=($getpage-1)*$recordsonpage;
		
		$matching .= "select * from tbl_putty where productType='Putty' ";

		$model2=Yii::app()->db->createCommand($matching)->queryAll();
		$totalrecords=count($model2);
		$pages2=ceil($totalrecords/$recordsonpage); 
		
		if(!empty($_GET['pricemin']))
		{
			$matching .=" and  price between '".$_GET['pricemin']."' and '".$_GET['pricemax']."' ";
		}
		
		if(!empty($_GET['search_text']))
		{
			$matching.=" and ( productType LIKE '%".addslashes($_GET['search_text'])."%' OR productName LIKE '%".addslashes($_GET['search_text'])."%' OR brandName LIKE '%".addslashes($_GET['search_text'])."%' OR product_detail LIKE '%".addslashes($_GET['search_text'])."%') ";
		}
		
		
		if(!empty($_GET['sorttype']))
		{
				if($_GET['sorttype']==1)
				{
				$matching .="    order by price asc ";
				}
				if($_GET['sorttype']==2)
				{
				$matching .="    order by price desc ";
				}


		}
		else
		{
			$matching .="   order by price asc ";
		}
		$matching .="  limit " . $start . ", " . $recordsonpage . "  ";
		
		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();


		$this->render('putty',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	} 
	
	public function actionSariya()
	{
		$recordsonpage=10;$matching="";$moesarray="";
		if(empty($_GET['page']))
		{
		$getpage=1;
		$url=Yii::app()->createUrl('site/sariya');	
		}else
		{
		$getpage=$_GET['page'];
		$url=Yii::app()->createUrl('site/sariya');	
		}
		$start=($getpage-1)*$recordsonpage;
		
		$matching .= "select * from tbl_sariya where productType='Sariya' ";

		$model2=Yii::app()->db->createCommand($matching)->queryAll();
		$totalrecords=count($model2);
		$pages2=ceil($totalrecords/$recordsonpage); 
		
		if(!empty($_GET['pricemin']))
		{
			$matching .=" and  price between '".$_GET['pricemin']."' and '".$_GET['pricemax']."' ";
		}
		
		if(!empty($_GET['search_text']))
		{
			$matching.=" and ( productType LIKE '%".addslashes($_GET['search_text'])."%' OR productName LIKE '%".addslashes($_GET['search_text'])."%' OR brandname LIKE '%".addslashes($_GET['search_text'])."%' OR product_detail LIKE '%".addslashes($_GET['search_text'])."%' OR material  LIKE '%".addslashes($_GET['search_text'])."%' OR thickness  LIKE '%".addslashes($_GET['search_text'])."%' OR grade  LIKE '%".addslashes($_GET['search_text'])."%') ";
		}
		
		
		if(!empty($_GET['sorttype']))
		{
				if($_GET['sorttype']==1)
				{
				$matching .="    order by price asc ";
				}
				if($_GET['sorttype']==2)
				{
				$matching .="    order by price desc ";
				}


		}
		else
		{
			$matching .="   order by price asc ";
		}
		$matching .="  limit " . $start . ", " . $recordsonpage . "  ";
		
		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();


		$this->render('sariya',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	} 
	
	public function actionTiles()
	{
		$recordsonpage=10;$matching="";$moesarray="";
		if(empty($_GET['page']))
		{
		$getpage=1;
		$url=Yii::app()->createUrl('site/tiles');	
		}else
		{
		$getpage=$_GET['page'];
		$url=Yii::app()->createUrl('site/tiles');	
		}
		$start=($getpage-1)*$recordsonpage;
		
		$matching .= "select * from tbl_tiles where productType='Tiles' ";

		$model2=Yii::app()->db->createCommand($matching)->queryAll();
		$totalrecords=count($model2);
		$pages2=ceil($totalrecords/$recordsonpage); 
		
		if(!empty($_GET['pricemin']))
		{
			$matching .=" and  price between '".$_GET['pricemin']."' and '".$_GET['pricemax']."' ";
		}
		
		if(!empty($_GET['search_text']))
		{
			$matching.=" and ( productType LIKE '%".addslashes($_GET['search_text'])."%' OR productname LIKE '%".addslashes($_GET['search_text'])."%' OR brandname LIKE '%".addslashes($_GET['search_text'])."%' OR product_detail LIKE '%".addslashes($_GET['search_text'])."%' OR usage_area  LIKE '%".addslashes($_GET['search_text'])."%' OR tileMaterial  LIKE '%".addslashes($_GET['search_text'])."%' OR box_size  LIKE '%".addslashes($_GET['search_text'])."%') ";
		}
		
		
		if(!empty($_GET['sorttype']))
		{
				if($_GET['sorttype']==1)
				{
				$matching .="    order by price asc ";
				}
				if($_GET['sorttype']==2)
				{
				$matching .="    order by price desc ";
				}


		}
		else
		{
			$matching .="   order by price asc ";
		}
		$matching .="  limit " . $start . ", " . $recordsonpage . "  ";
		
		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();


		$this->render('tiles',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	} 
	
	public function actionTilesDetail()
	{
		$tilesDetail=Tiles::model()->findByAttributes(array('productId'=>$_GET['tid']));
		$tiles = "select * from tbl_tiles order by rand() limit 0,2 ";
		$tilesadata=Yii::app()->db->createCommand($tiles)->queryAll();
		
		$this->render('tiles_detail',array('tilesDetail'=>$tilesDetail,'tilesadata'=>$tilesadata));
	}
	
	
	public function actionSariyaDetail()
	{
		$sariyaDetail=Sariya::model()->findByAttributes(array('productId'=>$_GET['sid']));
		$sariya = "select * from tbl_sariya order by rand() limit 0,2 ";
		$sariyadata=Yii::app()->db->createCommand($sariya)->queryAll();
		
		$this->render('sariya_detail',array('sariyaDetail'=>$sariyaDetail,'sariyadata'=>$sariyadata));
	}
	
	public function actionPuttyDetail()
	{
		$puttyDetail=Putty::model()->findByAttributes(array('productId'=>$_GET['puid']));
		$putty = "select * from  tbl_putty order by rand() limit 0,10 ";
		$puttydata=Yii::app()->db->createCommand($putty)->queryAll();
		
		$this->render('putty_detail',array('puttyDetail'=>$puttyDetail,'puttydata'=>$puttydata));
	}
	
	public function actionPopDetail()
	{
		$popDetail=Pop::model()->findByAttributes(array('productId'=>$_GET['poid']));
		$pop = "select * from  tbl_pop order by rand() limit 0,10 ";
		$popdata=Yii::app()->db->createCommand($pop)->queryAll();
		
		$this->render('pop_detail',array('popDetail'=>$popDetail,'popdata'=>$popdata));
	}
	
	
	public function actionMorangDetail()
	{
		$morangDetail=Morang::model()->findByAttributes(array('productId'=>$_GET['moid']));
		$morang = "select * from  tbl_morang order by rand() limit 0,2 ";
		$morangdata=Yii::app()->db->createCommand($morang)->queryAll();
		
		$this->render('morang_detail',array('morangDetail'=>$morangDetail,'morangdata'=>$morangdata));
	}
	
	
	public function actionPaintsDetail()
	{
		$paintDetail=Paints::model()->findByAttributes(array('productId'=>$_GET['pid']));
		$paints = "select * from  tbl_paints order by rand() limit 0,10 ";
		$paintsdata=Yii::app()->db->createCommand($paints)->queryAll();
		
		$this->render('paint_detail',array('paintDetail'=>$paintDetail,'paintsdata'=>$paintsdata));
	}
	
	public function actionMarbleDetail()
	{
		$marbleDetail=Marble::model()->findByAttributes(array('productId'=>$_GET['mid']));
		$marble = "select * from  tbl_marble order by rand() limit 0,10 ";
		$marbledata=Yii::app()->db->createCommand($marble)->queryAll();
		
		$this->render('marble_detail',array('marbleDetail'=>$marbleDetail,'marbledata'=>$marbledata));
	}
	
		
	public function actionGittiDetail()
	{
		$gittiDetail=Gitti::model()->findByAttributes(array('productId'=>$_GET['gid']));
		$gitti = "select * from  tbl_gitti order by rand() limit 0,10 ";
		$gittidata=Yii::app()->db->createCommand($gitti)->queryAll();
		
		$this->render('gitti_detail',array('gittiDetail'=>$gittiDetail,'gittidata'=>$gittidata));
	}


	public function actionCementDetail()
	{
		$cementDetail=Cement::model()->findByAttributes(array('productId'=>$_GET['cid']));
		$cement = "select * from  tbl_cement order by rand() limit 0,10 ";
		$cementdata=Yii::app()->db->createCommand($cement)->queryAll();
		
		$this->render('cement_detail',array('cementDetail'=>$cementDetail,'cementdata'=>$cementdata));
	}



	public function actionBrickDetail()
	{
		$brikDetail=Bricks::model()->findByAttributes(array('productId'=>$_GET['brid']));
		$brik = "select * from  tbl_bricks order by rand() limit 0,10 ";
		$brickdata=Yii::app()->db->createCommand($brik)->queryAll();
		
		$this->render('brick_detail',array('brikDetail'=>$brikDetail,'brickdata'=>$brickdata));
	}
	
	public function actionContact()
	
	{ $mesg="";
	
		if(!empty($_POST))
		
		{
		
		$message=$_POST['c_message']."<br>".$_POST['c_email'];
		$sub='New Message on From Visitor';
		
		$model1->sendEmail('infralogistics11@gmail.com', $sub, $message);
		
		$mesg="<font color=\"green\">Thank You For Your Feedback.</font>";
		
		}

	$this->render('contact',array('mesg'=>$mesg));

  }

	public function actionTerms()
	{
		$this->render('terms');
	}



	public function actionServices()
	{

		$this->render('services');

	}

	public function actionPrivacy()

	{

		$this->render('privacy');

	}

	public function actionAbout()
	{

		$this->render('aboutus');
	}


	public function actionSearch()

	{

		$error='';

		$recordsonpage=10;$matching="";$moesarray="";

		if(empty($_GET['page']))

		{

		$getpage=1;

		//$url="memberprofile.php?";

		$url=Yii::app()->createUrl('site/search?');	

		}else

		{

		$getpage=$_GET['page'];

		$url=Yii::app()->createUrl('site/search?');	

		}

		$start=($getpage-1)*$recordsonpage;

		

		$matching ="select * from  latest_news_tbl   ";

		

		if(!empty($_GET['search_text']))

			{

				

			

			 

			 $matching.=" where ( title LIKE '".$_REQUEST['search_text']."%'  OR shortcontent LIKE '%".$_REQUEST['search_text']."%'  OR content LIKE '%".$_REQUEST['search_text']."%' OR catName LIKE '".$_REQUEST['search_text']."%' ) ";

			 

			

				

		}

		

		$matching .="   order by views desc  ";

		

		$model2=Yii::app()->db->createCommand($matching)->queryAll();

		

		

		//echo $matching;

		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();

		$totalrecords=count($model2);

		$pages2=ceil($totalrecords/$recordsonpage); 

		

		$this->render('search',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	}

	

	

	public function actionPopularnews()

	{

		$error='';

		$recordsonpage=10;$matching="";$moesarray="";

		if(empty($_GET['page']))

		{

		$getpage=1;

		//$url="memberprofile.php?";

		$url=Yii::app()->createUrl('site/popularnews');	

		}else

		{

		$getpage=$_GET['page'];

		$url=Yii::app()->createUrl('site/popularnews');	

		}

		$start=($getpage-1)*$recordsonpage;

			//$matching .="select * from user_campaigns where campaign_title!='' and camp_status_coverimg='Yes' and cover_story='Yes' and account='yes' ORDER BY id DESC ";

		$matching ="select * from  latest_news_tbl order by views desc ";

		$model2=Yii::app()->db->createCommand($matching)->queryAll();

		$matching .="  limit " . $start . ", " . $recordsonpage . "  ";

		//echo $matching;

		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();

		$totalrecords=count($model2);

		$pages2=ceil($totalrecords/$recordsonpage); 

		$this->render('popularnews',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	}

	

	public function actionFeaturednews()

	{

		$error='';

		$recordsonpage=10;$matching="";$moesarray="";

		if(empty($_GET['page']))

		{

		$getpage=1;

		//$url="memberprofile.php?";

		$url=Yii::app()->createUrl('site/featurednews');	

		}else

		{

		$getpage=$_GET['page'];

		$url=Yii::app()->createUrl('site/featurednews');	

		}

		$start=($getpage-1)*$recordsonpage;

			//$matching .="select * from user_campaigns where campaign_title!='' and camp_status_coverimg='Yes' and cover_story='Yes' and account='yes' ORDER BY id DESC ";

		$matching ="select * from  latest_news_tbl where listtype='Featured' order by id desc ";

		$model2=Yii::app()->db->createCommand($matching)->queryAll();

		$matching .="  limit " . $start . ", " . $recordsonpage . "  ";

		//echo $matching;

		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();

		$totalrecords=count($model2);

		$pages2=ceil($totalrecords/$recordsonpage); 

		$this->render('featurednews',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	}

	

	

	public function actionLatest()

	{

		$error='';

		$recordsonpage=10;$matching="";$moesarray="";

		if(empty($_GET['page']))

		{

		$getpage=1;

		//$url="memberprofile.php?";

		$url=Yii::app()->createUrl('site/latest');	

		}else

		{

		$getpage=$_GET['page'];

		$url=Yii::app()->createUrl('site/latest');	

		}

		$start=($getpage-1)*$recordsonpage;

			//$matching .="select * from user_campaigns where campaign_title!='' and camp_status_coverimg='Yes' and cover_story='Yes' and account='yes' ORDER BY id DESC ";

		$matching ="select * from  latest_news_tbl order by id desc ";

		$model2=Yii::app()->db->createCommand($matching)->queryAll();

		$matching .="  limit " . $start . ", " . $recordsonpage . "  ";

		//echo $matching;

		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();

		$totalrecords=count($model2);

		$pages2=ceil($totalrecords/$recordsonpage); 

		$this->render('latest',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	}

	

	public function actionCategory()

	{

		$error='';

		$recordsonpage=10;$matching="";$moesarray="";

		if(empty($_GET['page']))

		{

		$getpage=1;

		//$url="memberprofile.php?";

		$url=Yii::app()->createUrl('site/category?id='.$_GET['id'].'&');	

		}else

		{

		$getpage=$_GET['page'];

		$url=Yii::app()->createUrl('site/category?id='.$_GET['id'].'&');	

		}

		$start=($getpage-1)*$recordsonpage;

			//$matching .="select * from user_campaigns where campaign_title!='' and camp_status_coverimg='Yes' and cover_story='Yes' and account='yes' ORDER BY id DESC ";

		$matching .= "select * from  latest_news_tbl where catName='".$_GET['id']."'  order by id desc";

		$model2=Yii::app()->db->createCommand($matching)->queryAll();

		$matching .="  limit " . $start . ", " . $recordsonpage . "  ";

		//echo $matching;

		$mtchdata=Yii::app()->db->createCommand($matching)->queryAll();

		$totalrecords=count($model2);

		$pages2=ceil($totalrecords/$recordsonpage); 

		$this->render('category',array('mtchdata'=>$mtchdata,'getpage'=>$getpage,'pages2'=>$pages2,'url'=>$url,'totalrecords'=>$totalrecords));

	}

	

	}

?>



