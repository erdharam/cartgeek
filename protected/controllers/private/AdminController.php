<?php
class AdminController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	public $layout='//layouts/adminmain';

	/**
	 * @return array action filters
	 */

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	/**
	 * Declares class-based actions.
	 */
	

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','login'),
				'users'=>array('*'),
			),
					
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','home','error'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

		
    public function loadModel($id)
	{
		$model=admin::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
		protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='admin-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	
	/**
	 * Manages all models.
	 */
	public function actionHome()
	{
		$model=new admin();
		
		
		
		$this->render('dashboard',array(
			'model'=>$model,
		
		));
	}
	
	public function actionAddbricks()
	{
		$model=new admin();
		
		$this->render('addbricks',array(
			'model'=>$model,
		));
	}
	
	
	public function actionIndex()
	{
		
		$this->layout='//layouts/adminlogin';
		$model=new admin;
		$message="";
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['admin']))
		{
			$model->attributes=$_POST['admin'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
			 { $this->redirect('home');
			 
			 }
			 else
			 {
				 $message='<font color="red">Either Username or Password is Wrong!</font>';
			 }
		
		}
		
	
		$this->render('index',array('model'=>$model,'message'=>$message));
	

	}
	
	
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	
	public function actionSetting()
	{
		$this->layout = '//layouts/main';
		$model=new Site;
		if(isset($_POST['Site']))
		{
			$model->attributes=$_POST['Site'];
			if($model->save())
			{
				Yii::app()->user->setFlash('setting','Thank you for updating.');
				$this->refresh();
			}
		}
		$this->render('setting',array('model'=>$model));
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect('index');
	}
	
}