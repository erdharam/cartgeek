<?php

class PropertylistController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/adminmain';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','block','unblock','pending','approve'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Propertylist;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Propertylist']))
		{
			$model->attributes=$_POST['Propertylist'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Propertylist']))
		{
			
			$model->attributes=$_POST['Propertylist'];
			if($model->save())
			$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
			$userdetail=Propertylist::model()->findByAttributes(array('id'=>$id));
	
	$subcatdatacit ="select * from  lko_property_images_tbl where pid='".$userdetail['listingId']."'";
	$cmslisting=Yii::app()->db->createCommand($subcatdatacit)->queryAll(); 
	
	foreach($cmslisting as $unlinkphoto)
	{
	unlink($unlinkphoto['originalImage']);
	unlink($unlinkphoto['imageThumb']);
	}
	
	
	$command4 = Yii::app()->db->createCommand("delete from lko_property_images_tbl where pid='".$userdetail['listingId']."' ");
	
	$row4=$command4->execute(); 
	

		
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Propertylist');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function actionBlock()
	{
		 $id=$_GET['id'];
		
		$model=Propertylist::model()->findByAttributes(array('id'=>$id));
		
		$model->featuredPost='No';
	
		$model->update();
	
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	public function actionUnblock()
	{
		 $id=$_GET['id'];
		
		$model=Propertylist::model()->findByAttributes(array('id'=>$id));
		//print_r($model);
		$model->featuredPost='Yes';
	
		$model->update();
	
	if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	public function actionPending()
	{
		 $id=$_GET['id'];
		
		$model=Propertylist::model()->findByAttributes(array('id'=>$id));
		
		$model->listingstatus='Pending';
	
		$model->update();
	
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	public function actionApprove()
	{
		 $id=$_GET['id'];
		 $model11 = New User(); 
		$model=Propertylist::model()->findByAttributes(array('id'=>$id));
		//print_r($model);
		$model->listingstatus='Approve';
	
		if($model->update())
		{$messageadmin ='<html xmlns=\"http://www.w3.org/1999/xhtml\">
						<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						</head>
						<body>
						
						Dear '.$model['sellerName '].', <br>
						
					  Your Business '.$model['adTitle '].' is now visible to users.<br><br>
						<br><br>
						
											
						Thank You For Choosing Lucknowbusiness.com<br>
						
	
						
						<b>Sincerely</b>,<br/>
						lucknowbusiness.com<br/><br/>
						
						
						
						</body></html>';
						 $model11->sendEmail($model['sellerEmail '], $sub, $message);
			
		}
		
		
	
	if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Propertylist('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Propertylist']))
			$model->attributes=$_GET['Propertylist'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Propertylist::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='propertylist-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
