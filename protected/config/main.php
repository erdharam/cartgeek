<?php
ini_set('max_execution_time',-1);
date_default_timezone_set('Asia/Calcutta');
error_reporting(0);

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Test',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),

	// application components
	'components'=>array(
	
	//'userCounter' => array(),

	
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			   
			//'urlSuffix'=>'.cms',
			'rules'=>array(
								
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
	
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
	
		// uncomment the following to use a MySQL database
		
/*
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=campamis_misDb',
			'emulatePrepare' => true,
			'username' => 'campamis_camUseR',
			'password' => 'mA3RaV.V(f@C',
			'charset' => 'utf8',
		),
		
*/

			'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=cartgeek_db',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		
		
		/*'db'=>array(
			 'class'=>'ext.oci8Pdo.OciDbConnection',
			'connectionString'=>'oci:dbname=182.69.119.250:1521/energymgmt;charset=UTF8',
			'emulatePrepare' => true,
			'username' => 'SYSTEM',
			'password' => 'Sta@12345',
			'charset' => 'utf8',
		),*/
		
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminemail'=>'webmaster@example.com',
	),
);