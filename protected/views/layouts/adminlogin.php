
 <!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo CHtml::encode($this->pageTitle); ?></title>
  <!-- base:css -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/admin_files/vendors/typicons/typicons.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/admin_files/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/admin_files/css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/admin_files/images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
       <?php echo $content; ?>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- base:js -->
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/admin_files/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
   <script src="<?php echo Yii::app()->request->baseUrl; ?>/admin_files/js/off-canvas.js"></script>
   <script src="<?php echo Yii::app()->request->baseUrl; ?>/admin_files/js/hoverable-collapse.js"></script>
   <script src="<?php echo Yii::app()->request->baseUrl; ?>/admin_files/js/template.js"></script>
   <script src="<?php echo Yii::app()->request->baseUrl; ?>/admin_files/js/settings.js"></script>
   <script src="<?php echo Yii::app()->request->baseUrl; ?>/admin_files/js/todolist.js"></script>
  <!-- endinject -->
</body>

</html>
