<?php
/* @var $this MylinksperpageController */
/* @var $model Mylinksperpage */
/* @var $form CActiveForm */
$this->pageTitle=Yii::app()->name . ' - Contact Email';
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mylinksperpage-form',
	'enableAjaxValidation'=>false,
)); ?>

	

	<?php echo $form->errorSummary($model); ?>

                <div id="page-content">
                    <!-- Wizard Header -->
                    <div class="content-header">
                        <div class="header-section">
                            <h1>
                                <i class="fa fa-magic"></i>Admin Panel<br><small>Update Your Email!</small>
                            </h1>
                        </div>
                    </div>
                 
                    <!-- END Wizard Header -->

                    <!-- Progress Bar Wizard Block -->
                    <div class="block">
                        <!-- Progress Bars Wizard Title -->
                        <div class="block-title">
                            <h2><strong>Manage Email</strong></h2>
                        </div>
                        <!-- END Progress Bar Wizard Title -->

                        <!-- Progress Bar Wizard Content -->
                        <div class="row">
                            
                            <div class="col-sm-6 col-sm-offset-1">
                                <!-- Wizard Progress Bar, functionality initialized in js/pages/formsWizard.js -->
                                
                                <!-- END Wizard Progress Bar -->

                                <!-- Progress Wizard Content -->
                                <form id="progress-wizard" action="" method="post" class="form-horizontal">
                                    <!-- First Step -->
                                    <div id="progress-first" class="step">
                                        <?php 
 $searchquery="select * from lko_mysite_linksperpage_tbl where type='email' ";
 $contentdata=Yii::app()->db->createCommand($searchquery)->queryAll();
 //$tot1=count($contentdata);
		foreach($contentdata as $searchpostvalue)
    	 { 
	 
	?>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="example-email">Email</label>
                                            <div class="col-md-8">
                                                <input type="text" id="example-progress-email" name="links" class="form-control" value="<?php echo $searchpostvalue['links'];?>"> <input type="hidden"  name="ides" value="<?php echo $searchpostvalue['id'];?>"/>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <!-- END First Step -->

                                    <!-- Second Step -->
                                    
                                    <!-- END Second Step -->

                                    <!-- Third Step -->
                                    
                                    <!-- END Third Step -->

                                    <!-- Form Buttons -->
                                    <div class="form-group form-actions">
                                        <div class="col-md-8 col-md-offset-4">
                                           <?php echo CHtml::submitButton('Update', array("class" => "btn btn-sm btn-primary")); ?>
                                          
                                        </div>
                                    </div>
                                    <!-- END Form Buttons -->
                                </form>
                                <!-- END Progress Wizard Content -->
                            </div>
                        </div>
                        <!-- END Progress Bar Wizard Content -->
                    </div>
                    <!-- END Progress Bar Wizard Block -->

                    <!-- Wizards Row -->
                    
                    <!-- END Wizards Row -->

                    <!-- Clickable Wizard Block -->
                    
                    <!-- END Clickable Wizard Block -->

                    <!-- Terms Modal -->
                    
                    <!-- END Terms Modal -->
                </div>
                <!-- END Page Content -->

                <!-- Footer -->
             
                <!-- END Footer -->
            
<?php $this->endWidget(); ?>
