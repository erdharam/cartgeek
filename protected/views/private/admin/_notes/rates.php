<?php
/* @var $this CmsController */
/* @var $model Cms */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
                                    'id'=>'cms-form',
                                    'enableAjaxValidation'=>false,
									'htmlOptions'=>array(
										'class'=>"form-horizontal",
										'enctype'=>'multipart/form-data'
										),
                                    )); ?>

	
 <?php //echo $form->errorSummary($model); ?>

                    <!-- Wizard Header -->
                    <div class="content-header">
                        <div class="header-section">
                            <h1>
                                <i class="fa fa-magic"></i>PROPERTY LISTING DETAILS<br><small>Easy steps to submit listing!</small>
                            </h1>
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Property Photos</li>
                        <li><a href="">Photo List</a></li>
                    </ul>
                    <!-- END Wizard Header -->

                    <!-- Progress Bar Wizard Block -->
                    
                    <!-- END Progress Bar Wizard Block -->

                    <!-- Wizards Row -->
                    <div class="row">
                        
                        <div class="col-md-12">
                            <!-- Wizard with Validation Block -->
                            <div class="block">
                                <!-- Wizard with Validation Title -->
                                <div class="block-title">
                                    <h2><strong>Photos </strong> List</h2>
                                </div>
                                <!-- END Wizard with Validation Title -->

                                <!-- Wizard with Validation Content -->
                                  <form action="" method="post" class="form-horizontal form-bordered">
                                    <!-- First Step -->
                                    <div id="advanced-first" class="step">
                                        <!-- Step Info -->
                                        <div class="wizard-steps">
                                            <div class="row">
                                                <div class="col-xs-2 active">
                                                    <span>General</span>
                                                </div>
                                                <div class="col-xs-2 active">
                                                    <span>Photos</span>
                                                </div>
                                                 <div class="col-xs-2 active">
                                                    <span>Additional</span>
                                                </div>
                                                 <div class="col-xs-2 active">
                                                    <span>Rates</span>
                                                </div>
                                                 <div class="col-xs-2">
                                                    <span>Availbility</span>
                                                </div>
                                                 <div class="col-xs-2">
                                                    <span>Opening Hours</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END Step Info -->
                                        <?php if (($prodetial['propertyType']=='Hotels') || ($prodetial['propertyType']=='Motels') || ($prodetial['propertyType']=='Resorts') || ($prodetial['propertyType']=='Vacation Rentals') || ($prodetial['propertyType']=='Camp Grounds') || ($prodetial['propertyType']=='Rv Resorts') || ($prodetial['propertyType']=='Boatel')){ ?>
                  
										<div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-email">Title<span class="text-danger">*</span></label>
                                        <div class="col-md-6">
                                            <input type="text" id="example-hf-email" name="title" class="form-control" required>
                                            <span class="help-block"></span>
                                        </div>
                                       </div>
                  
                   						 <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-email">Rate Period<span class="text-danger">*</span></label>
                                        <div class="col-md-6">
                                            <input type="text" style="width: 105px;" id="datepicker" name="from_date" class="form-control" value="" placeholder="Date" title="From" /> 
                                            <input type="text" name="to_date" id="datepickernew"  style="width: 105px;" class="form-control"  value="" title="To" placeholder="Date" />
                                            <span class="help-block"></span>
                                        </div>
                                       </div>
                    <!--Rates Table-->
                  

                      <thead>
                <tr id="ratesTableTitles">
                    <th class="alt">Rate Period</th>
                    <th>Nightly</th>
                    <th>Weekend Night</th>
                    <th>Weekly</th>
                    <th>Monthly</th>
                    <th>Event</th>
                    <th>Min. Stay</th>
                    <th>Action</th>
                </tr>
            </thead>        
                    <tr class="spacer"><td colspan="10">&nbsp;</td></tr>
					<tr class="ratePeriodLabel"><input type="hidden" id="propidold" name="propidold" value="<?php echo $propid;?>" />
                    <td class="alt"><div class="ratePeriodTitle"><input type="text" name="title" value="" placeholder="Title" /></div>
                    <div class="ratePeriodDates"><input type="text" style="width: 105px;" name="from_date" value="" placeholder="Date" title="From" /> 
                    <span style="color:#000">to</span> <input type="text" name="to_date" style="width: 105px;" value="" title="To" placeholder="Date" /></div></td>
					<td class="nightly"><div class="rate">$<input type="text" name="nightly" value=""  /></div></td>
					<td class="weekendNight"><div class="rate">$<input type="text" name="weekendNight" value=""  /></div></td>
					<td class="weekly"><div class="rate">$<input type="text" name="weekly" value=""  /></div></td>
					<td class="monthly"><div class="rate">$<input type="text" name="monthly" value=""  /></div></td>
					<td class="event"><div class="rate">$<input type="text" name="event" value=""  /></div></td>
                    <td class="min-stay"><div class="rate"><input type="text" name="min_stay" value=""  /> Nights</div></td>
                    <td class="min-stay"><div class="rate"><input type="submit" name="addvr" value="Add New" class="add" title="Add Rate"/></div></td>
                    </tr>
                    <tr class="spacer dividing"><td colspan="10">&nbsp;</td></tr>

           
     <?php } ?>
     
     
  
      </table>
                  </div>                      
                                 
                                    <!-- END First Step -->

                                    <!-- Second Step -->
                                    
                                    <!-- END Second Step -->

                                    <!-- Form Buttons -->
                                    <div class="form-group form-actions">
                                        <div class="col-md-8 col-md-offset-4">
                                            <input type="submit" class="btn btn-sm btn-warning" id="back2" value="Submit" name="submitone">
                                           
                                            
                                            <input type="hidden" id="propidold" name="propidold" value="<?php echo $propid;?>" />
                                        </div>
                                    </div>
                                    <!-- END Form Buttons -->
                                </form>
                                <!-- END Wizard with Validation Content -->
                            </div>
                            <!-- END Wizard with Validation Block -->
                        </div>
                    </div>
                    <!-- END Wizards Row -->
<div class="block full">
                        <div class="block-title">
                            <h2><strong>Photos</strong></h2>
                        </div>
                       

                        <div class="table-responsive">
                            <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center"><i class="gi gi-nameplate"></i></th>
                                        <th>Caption</th>
                                        <th>Order</th>
                                       <th>Sorting</th>
                                        <th class="text-center">Save Task</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                <?php
								//foreach($placedata as $photoslist)
								//{
									//$orders="";
								//$orders= PropertyImagesTbl::model()->findAllByAttributes(array('pid'=>$photoslist['pid']));
								?>
                                
                                    <!--<tr id="vidrating<?php //echo $photoslist['id'];?>">
                                        
                                        <td class="text-center"><img src="<?php //echo Yii::app()->request->baseUrl; ?>/<?php //echo $photoslist['imageThumb'];?>" alt="avatar" width="150" height="150"></td>
                                        <td><input type="name" name="imagecaption" class="form-control" id="caption<?php //echo $photoslist['id'];?>" value="<?php //
										//echo $photoslist['imageCaption'];?>"></td>
                                        <td><?php //echo $photoslist['imageOrder'];?></td>
                                        <td>
                                         <select name="neworder" id="neworder<?php //echo $photoslist['id'];?>">
                                         <option>Select</option>
										<?php //foreach($orders as $orderslist){?>
                                       <option value="<?php //echo //$orderslist['imageOrder'];?>" <?php //if($orderslist['imageOrder']==$photoslist['imageOrder']) { echo "selected"; } ?>><?php //echo $orderslist['imageOrder'];?></option>
                                        <?php //} ?>
                                        
                                        
                                        </select>
                                        </td>
                                        <td><a href="javascript:void(0)" data-toggle="tooltip" onclick="return savecaption('<?php //echo $photoslist['id'];?>');" title="Edit" class="btn btn-sm btn-warning">Save</a></td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                             
                                                <a href="javascript:void(0)" data-toggle="tooltip" onclick="return deleteimg('<?php //echo $photoslist['id'];?>')" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                  -->
                                  <?php //} ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Clickable Wizard Block -->
                    
                    <!-- END Clickable Wizard Block -->

                    <!-- Terms Modal -->
                    
                    <!-- END Terms Modal -->
             
<?php $this->endWidget(); ?>

<!-- form -->