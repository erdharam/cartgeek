<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<?php 
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'form-login',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions'=>array(
	'class'=>"form-horizontal form-bordered form-control-borderless"
	),
)); ?>
        <?php echo $message;?>
           <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                             <!--   <input type="text" id="login-email" name="login-email" class="form-control input-lg" placeholder="Email">-->
                                <?php echo $form->textField($model,'username', array('class' => 'form-control input-lg','placeholder'=>'Email')); ?>
                                 <?php echo $form->error($model,'username', array("class" => "err")); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                                <!--<input type="password" id="login-password" name="login-password" class="form-control input-lg" placeholder="Password">-->
                                 <?php echo $form->passwordField($model,'password', array('class' => 'form-control input-lg','placeholder'=>'Password')); ?>
                           <?php echo $form->error($model,'password', array("class" => "err")); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                       <!-- <div class="col-xs-4">
                            <label class="switch switch-primary" data-toggle="tooltip" title="Remember Me?">
                                <input type="checkbox" id="login-remember-me" name="login-remember-me" checked>
                                <span></span>
                            </label>
                        </div>-->
                        <div class="col-xs-8 text-right">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Login to Dashboard</button>
                        </div>
                         <div class="form-group">
                        <div class="col-xs-12 text-center">
                             <a href="javascript:void(0)" id="link-reminder-login"><small>Forgot password?</small></a>
                           
                        </div>
                    </div>
                    </div>
                  
             
                  <?php $this->endWidget(); ?> 
                  <form action="<?php echo Yii::app()->createUrl('private/admin/login'); ?>" method="post" id="form-reminder" class="form-horizontal form-bordered form-control-borderless display-none">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                <input type="email" id="reminder-email" name="reminderemail" class="form-control input-lg" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-xs-12 text-right">
                            <button type="submit" name="submit" value="forget" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Reset Password</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 text-center">
                            <small>Did you remember your password?</small> <a href="javascript:void(0)" id="link-reminder"><small>Login</small></a>
                        </div>
                    </div>
                </form>