<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<div class="content-wrapper d-flex align-items-center auth px-0">
  <div class="row w-100 mx-0">
    <div class="col-lg-4 mx-auto">
      <div class="auth-form-light text-left py-5 px-4 px-sm-5">
        <div class="brand-logo" align="center"> LOGO </div>
        <h4>Hello! let's get started</h4>
        <h6 class="font-weight-light">Sign in to continue.</h6>
        <?php 
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'form-login',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions'=>array(
	'class'=>"pt-3"
	),
)); ?>
         <?php echo $message;?>
        <div class="form-group"> <?php echo $form->textField($model,'username', array('class' => 'form-control form-control-lg','placeholder'=>'Email')); ?> <?php echo $form->error($model,'username', array("class" => "err")); ?> </div>
        <div class="form-group"> <?php echo $form->passwordField($model,'password', array('class' => 'form-control form-control-lg','placeholder'=>'Password')); ?> <?php echo $form->error($model,'password', array("class" => "err")); ?> </div>
        <div class="mt-3">
          <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" name="login">SIGN IN</button>
        </div>
        <?php $this->endWidget(); ?>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends --> 
