<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<?php 
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'form-login',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions'=>array(
	'class'=>"form-horizontal form-bordered form-control-borderless"
	),
)); ?>
        
           <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                             <!--   <input type="text" id="login-email" name="login-email" class="form-control input-lg" placeholder="Email">-->
                                <?php echo $form->textField($model,'username', array('class' => 'form-control input-lg','placeholder'=>'Email')); ?>
                                 <?php echo $form->error($model,'username', array("class" => "err")); ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group form-actions">
                   
                        <div class="col-xs-8 text-right">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i>Send</button>
                        </div>
                         <div class="form-group">
                        <div class="col-xs-12 text-center">
                            <a href="<?php echo Yii::app()->createUrl('private/admin/login'); ?>" id="link-reminder-login"><small>Login</small></a> 
                           
                        </div>
                    </div>
                    </div>
                  
             
                  <?php $this->endWidget(); ?> 