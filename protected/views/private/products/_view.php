<?php
/* @var $this ProductsController */
/* @var $model Products */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('productID')); ?>:</b>
	<?php echo CHtml::encode($data->productID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('productName')); ?>:</b>
	<?php echo CHtml::encode($data->productName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('productPrice')); ?>:</b>
	<?php echo CHtml::encode($data->productPrice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('proDescription')); ?>:</b>
	<?php echo CHtml::encode($data->proDescription); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('proStatus')); ?>:</b>
	<?php echo CHtml::encode($data->proStatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addedDate')); ?>:</b>
	<?php echo CHtml::encode($data->addedDate); ?>
	<br />


</div>