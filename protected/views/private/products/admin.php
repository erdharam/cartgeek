<?php
/* @var $this ProductsController */
/* @var $model Products */

$this->breadcrumbs=array(
	'Products'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Products', 'url'=>array('index')),
	array('label'=>'Create Products', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('products-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="main-panel">
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Products</h4>
          <div class="table-responsive">

<div class="search-form" style="float:left" >
<a href="<?php echo Yii::app()->createUrl('private/products/create'); ?>" class="btn btn-primary mr-2">Add New</a>
</div><!-- search-form -->


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'products-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		'id',
		'productID',
		'productName',
		'productPrice',
		'proDescription',
		
		/*
		'addedDate',
		*/
		array(
			'class'=>'CButtonColumn',
			'template' => '{update}{delete}',
			
		),
	),
)); ?>
 </div>
        </div>
      </div>
    </div>
  </div>
</div>

