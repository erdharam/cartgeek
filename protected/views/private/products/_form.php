<?php
/* @var $this ProductsController */
/* @var $model Products */
/* @var $form CActiveForm */


?>

<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">
      <div class="col-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Manage Products</h4>
            <p class="card-description"> <?php echo $error;?> </p>
            <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'vendor-form',
	'enableAjaxValidation'=>false,
	
'htmlOptions'=>array(
	'class'=>"pt-3",
	'name'=>'vendor-form',
	'enctype'=>'multipart/form-data'
	))); ?>
            <p class="note">Fields with <span style="color:red">*</span> are required.</p>
           <font id="errormain" style="display:none;color:red;font-size:12px;">Something Went Wrong.</font>
           <font id="sucess" style="display:none;color:green;font-size:15px;height:25px;border:1px solid green">Product Added.</font>
            <div class="form-group">
              <label for="exampleInputName1"><?php echo $form->labelEx($model,'productName'); ?></label>
              <?php echo $form->textArea($model,'productName',array('class'=>"form-control")); ?> 
			  <font color="red" id="error1" style="display:none;font-size:12px;">This field is required.</font>
  			  <font color="red" id="error2" style="display:none;font-size:12px;">Product Name should be minimum 5 characters.</font>
   </div>
            <div class="form-group">
              <label for="exampleInputName1"><?php echo $form->labelEx($model,'productPrice'); ?></label>
              <?php echo $form->textField($model,'productPrice',array('class'=>"form-control",'onkeypress'=>"return isNumberKey(event)" )); ?> 
              <font color="red" id="error3" style="display:none;font-size:12px;">This field is required.</font></div>
            <div class="form-group">
              <label for="exampleInputName1"><?php echo $form->labelEx($model,'proDescription'); ?></label>
              <?php echo $form->textArea($model,'proDescription',array('class'=>"form-control",'style'=>'height:200px')); ?> <font color="red" id="error4" style="display:none;font-size:12px;">This field is required.</font> </div>
              
                 <div class="form-group">
             <div id="dragAndDropFiles" class="uploadArea">
	<h1>Drop Files Here</h1>
</div><input type="file" name="multiUpload" style="display:none" id="multiUpload" multiple /></div>
              
              
       		<?php if(!empty($_GET['id'])) { ?>
            <div class="row buttons"> <button class='btn btn-primary mr-2' type="button" onClick="return updatepro();">Update</button>
            <input type="hidden" value="<?php echo $_GET['id']?>" id="ides">
            </div>
            <?php } else { ?>
            <div class="row buttons"> <button class='btn btn-primary mr-2' type="button" onClick="return savespro();">Save</button></div>
            <?php } ?>
            <?php $this->endWidget(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script src="https://cdn.tiny.cloud/1/zti00axju85l88pq3fq8v8qjc7f9l9gva9jdtpy4orf4rsg9/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

<script type="text/javascript">
function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
	   
tinymce.init({
   selector: 'textarea#Products_proDescription',
    menubar: false,
    plugins: "lists",
   // toolbar: 'forecolor | numlist bullist bold italic underline',
	toolbar: 'numlist bullist forecolor bold italic underline',
});


function updatepro()
    {
		var minLength = 5;
		var Products_productName=$("#Products_productName").val();
		var ides=$("#ides").val();
		var Products_productPrice=$("#Products_productPrice").val();
		var Products_proDescription = encodeURIComponent(tinyMCE.get('Products_proDescription').getContent());
		
		if(Products_productName=='')
		{
		$("#error1").show();
		$("#Products_productName").addClass("errorred");	
		return false;
		
		}
		else
		{
			$("#error1").hide();
			$("#Products_productName").removeClass("errorred");	
		
		}
		
		if (Products_productName.length < minLength)
		{
		$("#error2").show();
		$("#Products_productName").addClass("errorred");	
		return false;
		}
		else
		{
			$("#error2").hide();
			$("#Products_productName").removeClass("errorred");	
		
		}
		
		if(Products_productPrice=='')
		{
		$("#error3").show();
		$("#Products_productPrice").addClass("errorred");	
		return false;
		}
		else
		{
			$("#error3").hide();
			$("#Products_productPrice").removeClass("errorred");	
		
		}
		
		if(Products_proDescription=='')
		{
		$("#error4").show();
		$("#Products_proDescription").addClass("errorred");	
		return false;
		
		}
		else
		{
			$("#error4").hide();
			$("#Products_proDescription").removeClass("errorred");	
		
		}
		
		
		
		$.ajax({
		type: "POST",
		url: "<?php echo Yii::app()->createUrl('private/products/updateproduct'); ?>",
		data: "Products_productName="+ Products_productName + "&Products_productPrice=" + Products_productPrice + "&Products_proDescription=" + Products_proDescription + "&ides=" + ides,
		success: function(data){
		
		if(data=='no')
		{
		$("#errormain").show();
		}
		else
		{
		$("#sucess").show();
		
		   
		}
		}
		});
		return true;
    
   	
    }	
	
function savespro()
    {
		var minLength = 5;
		var Products_productName=$("#Products_productName").val();
		
		var Products_productPrice=$("#Products_productPrice").val();
		var Products_proDescription = encodeURIComponent(tinyMCE.get('Products_proDescription').getContent());
		
		if(Products_productName=='')
		{
		$("#error1").show();
		$("#Products_productName").addClass("errorred");	
		return false;
		
		}
		else
		{
			$("#error1").hide();
			$("#Products_productName").removeClass("errorred");	
		
		}
		
		if (Products_productName.length < minLength)
		{
		$("#error2").show();
		$("#Products_productName").addClass("errorred");	
		return false;
		}
		else
		{
			$("#error2").hide();
			$("#Products_productName").removeClass("errorred");	
		
		}
		
		if(Products_productPrice=='')
		{
		$("#error3").show();
		$("#Products_productPrice").addClass("errorred");	
		return false;
		}
		else
		{
			$("#error3").hide();
			$("#Products_productPrice").removeClass("errorred");	
		
		}
		
		if(Products_proDescription=='')
		{
		$("#error4").show();
		$("#Products_proDescription").addClass("errorred");	
		return false;
		
		}
		else
		{
			$("#error4").hide();
			$("#Products_proDescription").removeClass("errorred");	
		
		}
		
		
		
		$.ajax({
		type: "POST",
		url: "<?php echo Yii::app()->createUrl('private/products/saveproduct'); ?>",
		data: "Products_productName="+ Products_productName + "&Products_productPrice=" + Products_productPrice + "&Products_proDescription=" + Products_proDescription,
		success: function(data){
		
		if(data=='no')
		{
		$("#errormain").show();
		}
		else
		{
		$("#sucess").show();
		 $('#Products_productName').val('');
		  $('#Products_productPrice').val('');
		   $('#Products_proDescription').val('');
		   
		}
		}
		});
		return true;
    
   	
    }	
</script>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/admin_files/multiupload.js"></script>
<script type="text/javascript">
var config = {
	support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,",		// Valid file formats
	form: "vendor-form",					// Form ID
	dragArea: "dragAndDropFiles",		// Upload Area ID
	uploadUrl: "<?php echo Yii::app()->createUrl('private/products/dragdropfiles'); ?>"				// Server side upload url
}
$(document).ready(function(){
	initMultiUploader(config);
});
</script>
<style type="text/css">



.uploadArea{  /*min-height:300px;*/ height:160px; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}



h1, h5{ padding:0px; margin:0px; }



h1.title{ font-family:'Boogaloo', cursive; padding:10px; }



.uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}



.dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}



h5{ width:95%; line-height:25px;}



h5, h5 img {  float:left;  }



.buttonUpload { display:inline-block; padding: 4px 10px 4px; text-align: center; text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25); background-color: #0074cc; -webkit-border-radius: 4px;-moz-border-radius: 4px; border-radius: 4px; border-color: #e6e6e6 #e6e6e6 #bfbfbf; border: 1px solid #cccccc; color:#fff; }


.errorred {
        border: rgba(243,15,58,1.00) 1px solid !important;
    }
</style> 
